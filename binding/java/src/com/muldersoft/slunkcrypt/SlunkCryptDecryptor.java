/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt;

/**
 * SlunkCrypt <i>decryption</i> context.
 * <p>Wraps a &ldquo;native&rdquo; SlunkCrypt context that was initialized in <b><i>decryption</i></b> mode and implements the {@link com.muldersoft.slunkcrypt.SlunkCrypt SlunkCrypt} interface.</p>
 */
public class SlunkCryptDecryptor extends AbstractSlunkCrypt {

	/**
	 * Create a new {@link com.muldersoft.slunkcrypt.SlunkCrypt SlunkCrypt} instance in <b><i>decryption</i></b> mode.
	 * @param passwd the password to be used for decryption (UTF-8)
	 * @param nonce the nonce to be used for decryption
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 */
	public SlunkCryptDecryptor(final String passwd, final long nonce) throws SlunkCryptException {
		super(passwd, nonce, Mode.Decrypt);
	}

	/**
	 * Create a new {@link com.muldersoft.slunkcrypt.SlunkCrypt SlunkCrypt} instance in <b><i>decryption</i></b> mode.
	 * @param passwd the password to be used for decryption (binary)
	 * @param nonce the nonce to be used for decryption
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 */
	public SlunkCryptDecryptor(final byte[] passwd, final long nonce) throws SlunkCryptException {
		super(passwd, nonce, Mode.Decrypt);
	}

	/**
	 * Re-initialize this {@link com.muldersoft.slunkcrypt.SlunkCryptEncryptor SlunkCryptDecryptor} instance.
	 * @param passwd the password to be used for decryption (UTF-8)
	 * @param nonce the nonce to be used for decryption
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 * @throws IllegalStateException the instance has already been closed
	 */
	public void reset(final String passwd, final long nonce) throws SlunkCryptException {
		reset(passwd, nonce, Mode.Decrypt);
	}

	/**
	 * Re-initialize this {@link com.muldersoft.slunkcrypt.SlunkCryptEncryptor SlunkCryptDecryptor} instance.
	 * @param passwd the password to be used for decryption (binary)
	 * @param nonce the nonce to be used for decryption
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 * @throws IllegalStateException the instance has already been closed
	 */
	public void reset(final byte[] passwd, final long nonce) throws SlunkCryptException {
		reset(passwd, nonce , Mode.Decrypt);
	}
}
