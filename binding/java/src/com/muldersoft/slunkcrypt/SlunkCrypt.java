/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import com.muldersoft.slunkcrypt.internal.SlunkCryptLibrary;
import com.muldersoft.slunkcrypt.internal.types.UInt64ByReference;

/**
 * SlunkCrypt context for encryption or decryption.
 * <p>Implementations of this interface wrap the &ldquo;native&rdquo; SlunkCrypt context and they provide the instance methods for {@link SlunkCrypt#process(byte[]) processing}, i.e. encrypting or decrypting, chunks of data.</p>
 * <p>Separate implementations for {@link com.muldersoft.slunkcrypt.SlunkCryptEncryptor <i>encryption</i>} mode and {@link com.muldersoft.slunkcrypt.SlunkCryptDecryptor <i>decryption</i>} mode are available.</p>
 * <p><b><i>Warning:</i></b> The wrapped &ldquo;native&rdquo; SlunkCrypt context must be released by calling the {@link SlunkCrypt#close close()} method, or otherwise a resource leak is imminent! Using <a href="https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html" target="_blank">try-with-resources</a> is recommended.</p>
 * <h3>System properties:</h3>
 * <table summary="System properties" border>
 *     <tr>
 *         <th><b>Property</b></th>
 *         <th><b>Meaning</b></th>
 *     </tr>
 *     <tr>
 *         <td><tt>slunkcrypt.thread_count</tt></td>
 *         <td>The number of threads to be used internally by SlunkCrypt; a value of <b><tt>0</tt></b> means auto-detection (the default)</td>
 *     </tr>
 * </table>
 * <h2>Thread Safety</h2>
 * <p>Separate <tt>SlunkCrypt</tt> instances can safely be accessed from concurrent threads <i>without</i> the need for any kind if synchronization, provided that each instance is only accessed by the single thread that has created the respective instance. In order to <i>share</i> the same <tt>SlunkCrypt</tt> instance between concurrent threads, every access to the &ldquo;shared&rdquo; instance must be synchronized explicitly by the callers!</p>
 * <h2>Example</h2>
 * <pre>{@code
 * public class Main {
 * 
 *     private static String PASSPHRASE = "OrpheanBeholderScryDoubt";
 * 
 *     public static void main(String[] args) throws Exception {
 *         // Fill buffer with random data (plaintext)
 *         final byte[] buffer = new byte[64];
 *         ThreadLocalRandom.current().nextBytes(buffer);
 *             
 *         // Encrypt the data in-place
 *         final long nonce;
 *         try(final SlunkCryptEncryptor instance = new SlunkCryptEncryptor(PASSPHRASE)) {
 *             nonce = instance.getNonce();
 *             instance.inplace(buffer);
 *         }
 * 
 *         // Decrypt the data in-place
 *         try(final SlunkCryptDecryptor instance = new SlunkCryptDecryptor(PASSPHRASE, nonce)) {
 *             instance.inplace(buffer);
 *         }
 *     }
 * }}</pre>
 */
public interface SlunkCrypt extends AutoCloseable {

	/**
	 * Process, i&#46;e&#46; encrypt or decrypt, the next chunk of data.
	 * <p>This method is designed for stream processing. It is supposed to be called repeatedly, until all input data has been processed.</p>
	 * @param input the input buffer to be read; will <i>not</i> be modified
	 * @return the new buffer with the result
	 * @throws IllegalStateException the instance was already closed
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 * @throws IllegalStateException the instance has already been closed
	 */
	byte[] process(byte[] input) throws SlunkCryptException;

	/**
	 * Process, i&#46;e&#46; encrypt or decrypt, the next chunk of data.
	 * <p>This method is designed for stream processing. It is supposed to be called repeatedly, until all input data has been processed.</p>
	 * <p><i>Note:</i> The size of the output buffer must be at least the size of the input buffer!</p>
	 * @param input the input buffer to be read; will <i>not</i> be modified
	 * @param output the output buffer to write the result to
	 * @throws IllegalStateException the instance was already closed
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 * @throws IllegalStateException the instance has already been closed
	 */
	void process(byte[] input, byte[] output) throws SlunkCryptException;

	/**
	 * Process, i&#46;e&#46; encrypt or decrypt, the next chunk of data <i>in-place</i>.
	 * <p>This method is designed for stream processing. It is supposed to be called repeatedly, until all input data has been processed.</p>
	 * @param buffer the input/output buffer to be updated
	 * @throws IllegalStateException the instance was already closed
	 * @throws SlunkCryptException the operation failed or it was aborted
	 * @throws IllegalArgumentException invalid parameters detected
	 * @throws IllegalStateException the instance has already been closed
	 */
	void inplace(byte[] buffer) throws SlunkCryptException;

	/**
	 * Generate a new random nonce.
	 * <p><i>Note:</i> The nonce is generating using a &ldquo;strong&rdquo; cryptographically secure pseudorandom number generator.</p>
	 * @return The new nonce value
	 * @throws SlunkCryptException the operation failed or it was aborted
	 */
	public static long generateNonce() throws SlunkCryptException {
		final UInt64ByReference nonce = new UInt64ByReference();
		final int errorCode = SlunkCryptLibrary.INSTANCE.slunkcrypt_generate_nonce(nonce);
		if (!SlunkCryptException.isSuccess(errorCode)) {
			throw SlunkCryptException.mapErrorCode(errorCode, "Failed to generate nonce!");
		}
		return nonce.getValue();
	}

	/**
	 * Get the version of the &ldquo;native&rdquo; SlunkCrypt library.
	 * @return The version number as <tt>Map&lt;String, Short&gt;</tt>, containing the keys: <ul><li><tt>"slunkcrypt.version.major"</tt>,<li> <tt>"slunkcrypt.version.minor"</tt>, <li><tt>"slunkcrypt.version.patch"</tt></ul>
	 */
	public static Map<String, Short> getVersion() {
		final Map<String, Short> version = new LinkedHashMap<String, Short>(3);
		version.put(SlunkCryptLibrary.Version.MAJOR.getSymbolName().toLowerCase(Locale.US).replace('_', '.'), SlunkCryptLibrary.Version.MAJOR.getValue().shortValue());
		version.put(SlunkCryptLibrary.Version.MINOR.getSymbolName().toLowerCase(Locale.US).replace('_', '.'), SlunkCryptLibrary.Version.MINOR.getValue().shortValue());
		version.put(SlunkCryptLibrary.Version.PATCH.getSymbolName().toLowerCase(Locale.US).replace('_', '.'), SlunkCryptLibrary.Version.PATCH.getValue().shortValue());
		return Collections.unmodifiableMap(version);
	}

	/**
	 * Get the build string of the &ldquo;native&rdquo; SlunkCrypt library.
	 * @return The build string, containing the build time and date, e.g. <tt>"Jun 24 2024, 21:55:08"</tt>
	 */
	public static String getBuild() {
		return SlunkCryptLibrary.Version.BUILD.getValue();
	}

	/**
	 * Close the &ldquo;native&rdquo; SlunkCrypt context wrapped by this {@link com.muldersoft.slunkcrypt.SlunkCrypt SlunkCrypt} instance and release any system resources associated with it.
	 * <p><b><i>Warning:</i></b> Once this method has been called, this instance is left in an &ldquo;invalidated&rdquo; state and any attempt to process more data is going to throw an <tt>IllegalStateException</tt>!</p>
	 */
	@Override
	void close();
}
