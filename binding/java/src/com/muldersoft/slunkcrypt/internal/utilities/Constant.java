/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt.internal.utilities;

import java.lang.reflect.Proxy;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.WeakHashMap;

import com.muldersoft.slunkcrypt.internal.types.UInt16;
import com.sun.jna.Library;
import com.sun.jna.Library.Handler;
import com.sun.jna.NativeLibrary;
import com.sun.jna.Pointer;

public class Constant {

	private static final String UTF_8 = StandardCharsets.UTF_8.name();

	private static final Map<Library, NativeLibrary> NATIVE_LIBRARY = Collections.synchronizedMap(new WeakHashMap<Library, NativeLibrary>());

	protected final Pointer address;
	
	protected final String symbolName;

	protected Constant(final NativeLibrary nativeLibrary, final String symbolName) {
		this.symbolName = Objects.requireNonNull(symbolName);
		address = nativeLibrary.getGlobalVariableAddress(symbolName);
	}

	protected static NativeLibrary getNativeLibrary(final Library library) {
		return NATIVE_LIBRARY.computeIfAbsent(library, key -> ((Handler)Proxy.getInvocationHandler(key)).getNativeLibrary());
	}

	public static UInt16Const ofUInt16(final Library library, final String symbolName) {
		return new UInt16Const(getNativeLibrary(library), symbolName);
	}

	public static StrPtrConst ofStrPtr(final Library library, final String symbolName) {
		return new StrPtrConst(getNativeLibrary(library), symbolName);
	}

	public static class UInt16Const extends Constant {

		protected UInt16Const(final NativeLibrary nativeLibrary, final String symbolName) {
			super(nativeLibrary, symbolName);
		}

		public UInt16 getValue() {
			return UInt16.of(address.getShort(0L));
		}
	}

	public static class StrPtrConst extends Constant {

		protected StrPtrConst(final NativeLibrary nativeLibrary, final String symbolName) {
			super(nativeLibrary, symbolName);
		}

		public String getValue() {
			return address.getPointer(0L).getString(0L, UTF_8);
		}
	}

	public String getSymbolName() {
		return symbolName;
	}
}
