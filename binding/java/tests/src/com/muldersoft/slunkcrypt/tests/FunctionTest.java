/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

package com.muldersoft.slunkcrypt.tests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

import com.muldersoft.slunkcrypt.SlunkCrypt;
import com.muldersoft.slunkcrypt.SlunkCryptException;

@TestMethodOrder(OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FunctionTest {

	private String lastTestName = new String();

	@BeforeEach
	void init(final TestInfo testInfo) {
		final String name = testInfo.getTestMethod().map(method -> method.getName()).orElse("N/A");
		if (!name.equalsIgnoreCase(lastTestName)) {
			System.out.printf("-------- [ %s ] --------%n", lastTestName = name);
		} else {
			System.out.println("----");
		}
	}

	@Test
	@Order(1)
	void getVersionTest() {
		final Map<String, Short> versionInfo = SlunkCrypt.getVersion();
		assertNotNull(versionInfo);
		final Short major, minor, patch;
		assertNotNull(major = versionInfo.get("slunkcrypt.version.major"));
		assertNotNull(minor = versionInfo.get("slunkcrypt.version.minor"));
		assertNotNull(patch = versionInfo.get("slunkcrypt.version.patch"));
		System.out.printf("Version: %d.%d.%d%n", major.shortValue(), minor.shortValue(), patch.shortValue());
	}

	@Test
	@Order(2)
	void getBuildTest() {
		final String build = SlunkCrypt.getBuild();
		assertNotNull(build);
		assertFalse(build.isEmpty());
		System.out.printf("Build: \"%s\"%n", build);
	}

	@RepeatedTest(8)
	@Order(3)
	void generateNonceTest() throws SlunkCryptException {
		final long nonce_1 = SlunkCrypt.generateNonce();
		System.out.printf("Nonce: %016X%n", nonce_1);
		
		final long nonce_2 = SlunkCrypt.generateNonce();
		System.out.printf("Nonce: %016X%n", nonce_2);
		
		assertNotEquals(nonce_1, nonce_2);
	}
}
