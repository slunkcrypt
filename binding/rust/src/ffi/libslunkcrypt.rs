/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

pub type slunkcrypt_t = usize;

const NULL_POINTER: *const ::std::ffi::c_void = ::std::ptr::null();
pub const SLUNKCRYPT_NULL: slunkcrypt_t = unsafe { ::std::mem::transmute(NULL_POINTER) };

pub const SLUNKCRYPT_FALSE:   ::std::os::raw::c_int =  0;
pub const SLUNKCRYPT_TRUE:    ::std::os::raw::c_int =  1;
pub const SLUNKCRYPT_ENCRYPT: ::std::os::raw::c_int =  0;
pub const SLUNKCRYPT_DECRYPT: ::std::os::raw::c_int =  1;
pub const SLUNKCRYPT_SUCCESS: ::std::os::raw::c_int =  0;
pub const SLUNKCRYPT_FAILURE: ::std::os::raw::c_int = -1;
pub const SLUNKCRYPT_ABORTED: ::std::os::raw::c_int = -2;

pub const SLUNKCRYPT_PWDLEN_MIN: usize =   8;
pub const SLUNKCRYPT_PWDLEN_MAX: usize = 256;

pub const SLUNKCRYPT_PARAM_VERSION: u16 = 2;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct slunkparam_t {
    pub version: u16,
    pub thread_count: usize,
    pub legacy_compat: ::std::os::raw::c_int,
    pub debug_logging: ::std::os::raw::c_int,
}

extern "C" {
    pub static SLUNKCRYPT_VERSION_MAJOR: u16;
    pub static SLUNKCRYPT_VERSION_MINOR: u16;
    pub static SLUNKCRYPT_VERSION_PATCH: u16;
    pub static SLUNKCRYPT_BUILD: *const ::std::os::raw::c_char;
    pub static SLUNKCRYPT_HAVE_THREADS: ::std::os::raw::c_int;
    pub static mut g_slunkcrypt_abort_flag: ::std::os::raw::c_int;

    pub fn slunkcrypt_generate_nonce(nonce: *mut u64) -> ::std::os::raw::c_int;
    pub fn slunkcrypt_alloc_ext(nonce: u64, passwd: *const u8, passwd_len: usize, mode: ::std::os::raw::c_int, param: *const slunkparam_t) -> slunkcrypt_t;
    pub fn slunkcrypt_reset(context: slunkcrypt_t, nonce: u64, passwd: *const u8, passwd_len: usize, mode: ::std::os::raw::c_int) -> ::std::os::raw::c_int;
    pub fn slunkcrypt_free(context: slunkcrypt_t);
    pub fn slunkcrypt_process(context: slunkcrypt_t, input: *const u8, output: *mut u8, length: usize) -> ::std::os::raw::c_int;
    pub fn slunkcrypt_inplace(context: slunkcrypt_t, buffer: *mut u8, length: usize) -> ::std::os::raw::c_int;
    pub fn slunkcrypt_random_bytes(buffer: *mut u8, length: usize) -> usize;
    pub fn slunkcrypt_bzero(buffer: *mut ::std::os::raw::c_void, length: usize);

}
