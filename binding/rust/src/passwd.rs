/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

/// Passphrase to be used with [SlunkCrypt](crate::SlunkCrypt) encryption or decryption.
pub enum SlunkCryptPasswd<'a> {
    /// A passphrase constructed from a `&str`
    Str(&'a str),
    /// A passphrase constructed from a `String`
    String(String),
    /// A passphrase constructed from `&[u8]`
    Bytes(&'a[u8]),
    /// A passphrase constructed from `Vec<u8>`
    Vec(Vec<u8>)
}

impl<'a> SlunkCryptPasswd<'a> {
    pub fn len(&self) -> usize {
        match self {
            Self::Str(str)    => str.len(),
            Self::String(str) => str.len(),
            Self::Bytes(bin)  => bin.len(),
            Self::Vec(bin)    => bin.len()
        }
    }

    pub fn as_bytes(&'a self) -> &'a[u8] {
        match self {
            Self::Str(str)    => str.as_bytes(),
            Self::String(str) => str.as_bytes(),
            Self::Bytes(bin)  => bin,
            Self::Vec(bin)    => bin.as_slice()
        }
    }
}
