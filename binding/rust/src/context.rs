/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

use std::num::NonZeroUsize;
use std::ptr;

use crate::error::{SlunkCryptError, SlunkCryptErrorKind};
use crate::functions::generate_nonce;
use crate::ffi::libslunkcrypt::{self, slunkcrypt_t, slunkparam_t, SLUNKCRYPT_SUCCESS, SLUNKCRYPT_NULL};
use crate::passwd::SlunkCryptPasswd;

/// SlunkCrypt context for encryption or decryption.
/// 
/// This struct wraps the "native" SlunkCrypt context. It provides functions for allocating a new encryption or decryption context as well as functions for processing, i.e. encrypting or decrypting, the next chunk of data within a specific `SlunkCrypt` context.
/// 
/// The wrapped "native" SlunkCrypt context is freed automatically when this struct is dropped.
///
/// # Thread safety
/// 
/// Separate `SlunkCrypt` contexts can safely be accessed from concurrent threads *without* the need for any kind if synchronization, provided that each context is only accessed by the *single* thread that has allocated the respective context.
/// 
/// # Example
/// ```
/// use slunkcrypt_rs::{SlunkCrypt, SlunkCryptPasswd};
/// use hex;
/// use rand::{thread_rng, RngCore};
/// use std::mem;
/// 
/// fn main() {
///     // Create passphrase from string
///     const PASSPHRASE: SlunkCryptPasswd = SlunkCryptPasswd::Str("OrpheanBeholderScryDoubt");
/// 
///     // Fill buffer with random data (plaintext)
///     let mut buffer = [ 0u8; 64 ];
///     thread_rng().fill_bytes(&mut buffer);
///     println!("Plaintext: {}", hex::encode(&buffer));
/// 
///     // Encrypt the data in-place
///     let (mut context_enc, nonce) = SlunkCrypt::init_encrypt(&PASSPHRASE, None).unwrap();
///     context_enc.inplace(&mut buffer).unwrap();
///     mem::drop(context_enc);
///     println!("Encrypted: {}", hex::encode(&buffer));
/// 
///     // Decrypt the data in-place
///     let mut context_dec = SlunkCrypt::init_decrypt(&PASSPHRASE, nonce, None).unwrap();
///     context_dec.inplace(&mut buffer).unwrap();
///     mem::drop(context_dec);
///     println!("Decrypted: {}", hex::encode(&buffer));
/// }
/// ```
#[derive(Debug)]
pub struct SlunkCrypt {
    context: slunkcrypt_t
}

impl SlunkCrypt {
    /// Create a new SlunkCrypt context and initialize it for the specified mode with the given password and the given nonce.
    /// 
    /// If successful, returns the new context; otherwise returns an [error code](SlunkCryptError).
    fn new(passwd: &SlunkCryptPasswd, nonce: u64, mode: i32, threads: Option<NonZeroUsize>) -> Result<Self, SlunkCryptError> {
        if passwd.len() < libslunkcrypt::SLUNKCRYPT_PWDLEN_MIN {
            return Err(SlunkCryptError::new(SlunkCryptErrorKind::Invalid, "Passphrase is too short!"));
        }
        if passwd.len() > libslunkcrypt::SLUNKCRYPT_PWDLEN_MAX {
            return Err(SlunkCryptError::new(SlunkCryptErrorKind::Invalid, "Passphrase is too long!"));
        }

        let param = slunkparam_t {
            version: libslunkcrypt::SLUNKCRYPT_PARAM_VERSION,
            thread_count: threads.map_or(usize::default(), NonZeroUsize::get),
            legacy_compat: i32::default(),
            debug_logging: i32::default()
        };

        let context: slunkcrypt_t = unsafe {
            libslunkcrypt::slunkcrypt_alloc_ext(nonce, passwd.as_bytes().as_ptr() as *const u8, passwd.len(), mode, &param)
        };

        if context == SLUNKCRYPT_NULL {
            Err(SlunkCryptError::new(SlunkCryptErrorKind::Failure, "failed to allocate slunkcrypt encryption context!"))
        } else {
            Ok(Self { context })
        }
    }

    /// Create a new SlunkCrypt context and initialize it for *encryption* mode with the given password and a fresh random nonce.
    /// 
    /// If successful, returns the new context and the generated nonce; otherwise returns an [error code](SlunkCryptError).
    pub fn init_encrypt(passwd: &SlunkCryptPasswd, threads: Option<NonZeroUsize>) -> Result<(Self, u64), SlunkCryptError> {
        let nonce = generate_nonce()?;
        match Self::new(passwd, nonce, libslunkcrypt::SLUNKCRYPT_ENCRYPT, threads) {
            Ok(context) => Ok((context, nonce)),
            Err(error) => Err(error)
        }
    }

    /// Create a new SlunkCrypt context and initialize it for *decryption* mode with the given password and the given nonce.
    /// 
    /// If successful, returns the new context; otherwise returns an [error code](SlunkCryptError).
    pub fn init_decrypt(passwd: &SlunkCryptPasswd, nonce: u64, threads: Option<NonZeroUsize>) -> Result<Self, SlunkCryptError> {
        Self::new(passwd, nonce, libslunkcrypt::SLUNKCRYPT_DECRYPT, threads)
    }

    pub fn reset_encrypt(&mut self, passwd: &SlunkCryptPasswd) -> Result<u64, SlunkCryptError> {
        if passwd.len() < libslunkcrypt::SLUNKCRYPT_PWDLEN_MIN {
            return Err(SlunkCryptError::new(SlunkCryptErrorKind::Invalid, "Passphrase is too short!"));
        }
        if passwd.len() > libslunkcrypt::SLUNKCRYPT_PWDLEN_MAX {
            return Err(SlunkCryptError::new(SlunkCryptErrorKind::Invalid, "Passphrase is too long!"));
        }

        let nonce = generate_nonce()?;

        let retval = unsafe {
            libslunkcrypt::slunkcrypt_reset(self.context, nonce, passwd.as_bytes().as_ptr(), passwd.len(), libslunkcrypt::SLUNKCRYPT_ENCRYPT)
        };
        match retval {
            SLUNKCRYPT_SUCCESS => Ok(nonce),
            _ => Err(SlunkCryptError::from_retval(retval, "failed to reset encryption context!"))
        }
    }

    pub fn reset_decrypt(&mut self, passwd: &SlunkCryptPasswd, nonce: u64) -> Result<(), SlunkCryptError> {
        if passwd.len() < libslunkcrypt::SLUNKCRYPT_PWDLEN_MIN {
            return Err(SlunkCryptError::new(SlunkCryptErrorKind::Invalid, "Passphrase is too short!"));
        }
        if passwd.len() > libslunkcrypt::SLUNKCRYPT_PWDLEN_MAX {
            return Err(SlunkCryptError::new(SlunkCryptErrorKind::Invalid, "Passphrase is too long!"));
        }

        let retval = unsafe {
            libslunkcrypt::slunkcrypt_reset(self.context, nonce, passwd.as_bytes().as_ptr(), passwd.len(), libslunkcrypt::SLUNKCRYPT_DECRYPT)
        };
        match retval {
            SLUNKCRYPT_SUCCESS => Ok(()),
            _ => Err(SlunkCryptError::from_retval(retval, "failed to reset decryption context!"))
        }
    }

    /// Process the next chunk of data. The given input data is encrypted or decrypted into the output buffer.
    /// 
    /// Does **not** modify the data in the input buffer.
    /// 
    /// If successful, returns nothing; otherwise returns an [error code](SlunkCryptError).
    pub fn process(&mut self, data_in: &[u8], data_out: &mut[u8]) -> Result<(), SlunkCryptError> { 
        if data_out.len() < data_in.len() {
            return Err(SlunkCryptError::new(SlunkCryptErrorKind::Invalid, "The output buffer is too small!"));
        }
        let retval = unsafe {
            libslunkcrypt::slunkcrypt_process(self.context, data_in.as_ptr(), data_out.as_mut_ptr(), data_in.len())
        };
        match retval {
            SLUNKCRYPT_SUCCESS => Ok(()),
            _ => Err(SlunkCryptError::from_retval(retval, "failed to process data!"))
        }
    }

    /// Process the next chunk of data. The given data is encrypted or decrypted *in-place*.
    /// 
    /// If successful, returns nothing; otherwise returns an [error code](SlunkCryptError).
    pub fn inplace(&mut self, data: &mut[u8]) -> Result<(), SlunkCryptError> { 
        let retval = unsafe {
            libslunkcrypt::slunkcrypt_inplace(self.context, data.as_mut_ptr(), data.len())
        };
        match retval {
            SLUNKCRYPT_SUCCESS => Ok(()),
            _ => Err(SlunkCryptError::from_retval(retval, "failed to process data!"))
        }
    }
}

impl Drop for SlunkCrypt {
    /// Free the SlunkCrypt context and release all of its associated ressources.
    fn drop(&mut self) {
        if self.context != SLUNKCRYPT_NULL {
            unsafe {
                libslunkcrypt::slunkcrypt_free(self.context);
            }
            let x : *const std::ffi::c_void = ptr::null();
            self.context = x as slunkcrypt_t;
        }
    }
}
