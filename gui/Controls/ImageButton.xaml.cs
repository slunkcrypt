﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace com.muldersoft.slunkcrypt.gui.ctrls
{
    /// <summary>
    /// Interaction logic for ImageToggleButton.xaml
    /// </summary>
    public partial class ImageButton : UserControl
    {
        public event RoutedEventHandler Clicked;

        public ImageButton()
        {
            InitializeComponent();
            Button.Click += ButtonClicked;
        }

        public ImageSource ImageSource
        {
            get
            {
                return ButtonImage.Source;
            }
            set
            {
                ButtonImage.Source = value;
            }
        }

        public string ButtonToolTip
        {
            get
            {
                return ButtonImage.ToolTip as string;
            }
            set
            {
                ButtonImage.ToolTip = value;
            }
        }

        private void ButtonClicked(object sender, RoutedEventArgs e)
        {
            Clicked?.Invoke(this, e);
        }
    }
}
