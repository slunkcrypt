﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace com.muldersoft.slunkcrypt.gui.ctrls
{
    public partial class PasswordToggleBox : UserControl
    {
        public event KeyEventHandler Entered;

        public static readonly DependencyProperty IsRevealedProperty = DependencyProperty.Register("IsRevealed", typeof(bool), typeof(PasswordToggleBox), new PropertyMetadata(false, OnIsRevealedPropertyChanged));

        public PasswordToggleBox()
        {
            InitializeComponent();
        }

        public Thickness EditPadding
        {
            get
            {
                return Edit_Hidden.Padding;
            }
            set
            {
                Edit_Hidden.Padding = Edit_Normal.Padding = value;
            }
        }

        public FontFamily EditFontFamily
        {
            get
            {
                return Edit_Hidden.FontFamily;
            }
            set
            {
                Edit_Normal.FontFamily = Edit_Hidden.FontFamily = value;
            }
        }

        public char PasswordChar
        {
            get
            {
                return Edit_Hidden.PasswordChar;
            }
            set
            {
                Edit_Hidden.PasswordChar = value;
            }
        }

        public int MaxLength
        {
            get
            {
                return Edit_Hidden.MaxLength;
            }
            set
            {
                Edit_Normal.MaxLength = Edit_Hidden.MaxLength = value;
            }
        }

        public bool IsRevealed
        {
            get
            {
                return (bool) GetValue(IsRevealedProperty);
            }
            set
            {
                SetValue(IsRevealedProperty, value);
            }
        }

        public string Password
        {
            get
            {
                return IsRevealed ? Edit_Normal.Text : Edit_Hidden.Password;
            }
            set
            {
                if (IsRevealed)
                {
                    Edit_Normal.Text = string.IsNullOrEmpty(value) ? string.Empty : value;
                }
                else
                {
                    Edit_Hidden.Password = string.IsNullOrEmpty(value) ? string.Empty : value;
                }
            }
        }

        public void Clear()
        {
            Edit_Hidden.Clear();
            Edit_Normal.Clear();
        }

        public void SelectAll()
        {
            if (IsRevealed)
            {
                Edit_Normal.SelectAll();
            }
            else
            {
                Edit_Hidden.SelectAll();
            }
        }

        private void OnIsRevealedPropertyChanged(bool value)
        {
            Panel.SetZIndex(Edit_Hidden, value ? 0 : 1);
            Panel.SetZIndex(Edit_Normal, value ? 1 : 0);
            if (value)
            {
                Edit_Normal.Text = Edit_Hidden.Password;
                Edit_Hidden.Clear();
            }
            else
            {
                Edit_Hidden.Password = Edit_Normal.Text;
                Edit_Normal.Clear();
            };
        }

        private static void OnIsRevealedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordToggleBox ptb = d as PasswordToggleBox;
            if (!ReferenceEquals(ptb, null))
            {
                bool? newValue = e.NewValue as bool?;
                if (newValue.HasValue)
                {
                    ptb.OnIsRevealedPropertyChanged(newValue.Value);
                }
            }
        }

        private void Edit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Entered?.Invoke(this, e);
                e.Handled = true;
            }
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            if (IsRevealed)
            {
                Edit_Normal.Focus();
            }
            else
            {
                Edit_Hidden.Focus();
            }
            e.Handled = true;
        }
    }
}
