﻿/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;

using static com.muldersoft.slunkcrypt.gui.Properties._Version;

[assembly: AssemblyTitle("SlunkCrypt GUI")]
[assembly: AssemblyDescription("SlunkCrypt GUI")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Muldersoft")]
[assembly: AssemblyProduct("SlunkCrypt")]
[assembly: AssemblyCopyright("Created by LoRd_MuldeR <MuldeR2@GMX.de>")]
[assembly: AssemblyTrademark("Muldersoft")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]

[assembly: AssemblyVersion(VERS_MAJOR + "." + VERS_MINOR + ".*")]
[assembly: AssemblyFileVersion(VERS_MAJOR + "." + VERS_MINOR + ".0." + VERS_PATCH)]
[assembly: AssemblyInformationalVersion(VERS_MAJOR + "." + VERS_MINOR + ".0." + VERS_PATCH)]
