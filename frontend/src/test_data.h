/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKAPP_TEST_DATA_H
#define INC_SLUNKAPP_TEST_DATA_H

#include "platform.h"

extern const char* const TEST_DATA_0;
extern const char* const TEST_DATA_1;
extern const char* const TEST_DATA_2;
extern const char* const TEST_DATA_3;

extern const uint64_t TEST_CHCK_ORIG_0, TEST_CHCK_ENCR_0[4U];
extern const uint64_t TEST_CHCK_ORIG_1, TEST_CHCK_ENCR_1[4U];
extern const uint64_t TEST_CHCK_ORIG_2, TEST_CHCK_ENCR_2[4U];
extern const uint64_t TEST_CHCK_ORIG_3, TEST_CHCK_ENCR_3[4U];

#endif
