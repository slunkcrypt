/*
   This is a stripped-down and simplified "64-Bit only" version of BLAKE2s
   BLAKE2 reference source code package - reference C implementations
   Copyright 2012, Samuel Neves <sneves@dei.uc.pt>.  You may use this under the
   terms of the CC0, the OpenSSL Licence, or the Apache Public License 2.0, at
   your option.  The terms of these licenses can be found at:
   - CC0 1.0 Universal : http://creativecommons.org/publicdomain/zero/1.0
   - OpenSSL license   : https://www.openssl.org/source/license.html
   - Apache 2.0        : http://www.apache.org/licenses/LICENSE-2.0
   More information about the BLAKE2 hash function can be found at
   https://blake2.net.
*/

#ifndef INC_SLUNKAPP_BLAKE2_H
#define INC_SLUNKAPP_BLAKE2_H

#include "platform.h"

#define BLAKE2S_BLOCKBYTES 64

typedef struct _blake2s_t
{
	uint32_t h[8], t[2], f[2];
	uint8_t  buf[BLAKE2S_BLOCKBYTES];
	size_t   buflen;
	uint8_t  last_node;
}
blake2s_t;

/*BLAKE2s stream API*/
void blake2s_init(blake2s_t*const S);
void blake2s_update(blake2s_t*const S, const void* const pin, size_t inlen);
uint64_t blake2s_final(blake2s_t*const S);

/*BLAKE2s at once*/
uint64_t blake2s_compute(const void* const pin, const size_t inlen);

#endif
