/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKAPP_UTILS_H
#define INC_SLUNKAPP_UTILS_H

#include "platform.h"

typedef void (signal_handler_t)(int);

void init_terminal(void);
void setup_signal_handler(const int signo, signal_handler_t* const handler);

uint64_t clock_freq(void);
uint64_t clock_read(void);

void store_ui64(uint8_t* const dst, const uint64_t value);
uint64_t load_ui64(const uint8_t* const src);
size_t fwrite_ui64(const uint64_t value, FILE *const stream);
size_t fread_ui64(uint64_t *const value, FILE *const stream);

char* CHR_to_utf8(const CHR *const input);

int same_file(const CHR*const path0, const CHR*const path1);
uint64_t get_size(FILE *const file);
const CHR *get_file_name(const CHR *path);

uint64_t round_down(const uint64_t value, const uint64_t base);

#define ARRAY_SIZE(X) (sizeof((X)) / sizeof(*(X)))
#define BOUND(MIN,VAL,MAX) (((VAL) < (MIN)) ? (MIN) : (((VAL) > (MAX)) ? (MAX) : (VAL)))
#define STARTS_WITH(X,Y) (!STRNICMP((X), (Y), STRLEN((Y))))
#define GET_LOWBITS(X) ((X) & 0x07)
#define SET_LOWBITS(X,Y) do { X = ((X) & 0xF8) | ((Y) & 0x07); } while(0)

#endif
