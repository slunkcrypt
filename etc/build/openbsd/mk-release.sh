#!/bin/ksh
set -e
cd -- "$(dirname -- "${0}")/../../.."

if [ -z "${cc_path}" ]; then
	cc_path="/usr/local/bin/egcc"
fi

mk_slunk() {
	gmake -B CC="${cc_path}" MARCH=${2} MTUNE=${3} STATIC=1 STRIP=1 FLTO=1
	cp -vf "frontend/bin/slunkcrypt" "out/slunkcrypt-${1}"
}

if [ "$(uname -s)" != "OpenBSD" ]; then
	echo "This script is supposed to run on the OpenBSD platform !!!"
	exit 1
fi

rm -rf "out" && mkdir -p "out"

case "$(uname -m)" in
	amd64)
		mk_slunk "x86_64" "x86-64" "nocona"
		;;
	i386)
		mk_slunk "i686" "pentiumpro" "pentium3"
		;;
	*)
		echo "Unknown host CPU type !!!"
		exit 1
esac

./etc/build/build_info.sh "${cc_path}" > "out/.build_info"

echo "Build completed successfully."
