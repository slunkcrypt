#!/bin/bash
set -e
cd -- "$(dirname -- "${BASH_SOURCE[0]}")/../../.."

mk_slunk() {
	make -B CC=${1} MARCH=${3} MTUNE=${4} STATIC=1 STRIP=1
	cp -vf "frontend/bin/slunkcrypt" "out/slunkcrypt-${2}"
}

if [ "$(uname -s)" != "Haiku" ]; then
	echo "This script is supposed to run on the SunOS (Solaris) platform !!!"
	exit 1
fi

rm -rf "out" && mkdir -p "out"

case "${BE_HOST_CPU}" in
	x86_64)
		mk_slunk "gcc" "x86_64" "x86-64" "nocona"
		./etc/build/build_info.sh "gcc" > "out/.build_info"
		;;
	x86)
		mk_slunk "gcc-x86" "i686" "pentiumpro" "pentium3"
		./etc/build/build_info.sh "gcc-x86" > "out/.build_info"
		;;
	*)
		echo "Unknown host CPU type !!!"
		exit 1
esac

echo "Build completed successfully."
