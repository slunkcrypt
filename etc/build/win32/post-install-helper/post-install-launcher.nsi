!include FileFunc.nsh
!include WinVer.nsh

Unicode true
XPStyle on
ManifestSupportedOS all
RequestExecutionLevel user
AutoCloseWindow true
InstallColors FFD74A 000000
ShowInstDetails show

OutFile "bin\post-install-launcher.exe"
Icon "post-install-launcher.ico"
ChangeUI all "${NSISDIR}\Contrib\UIs\sdbarker_tiny.exe"

Caption "Almost there..."
SubCaption 1 " "
SubCaption 2 " "
SubCaption 3 " "
SubCaption 4 " "

!macro PrintStatusMessage message
	SetDetailsPrint both
	DetailPrint "${message}"
	SetDetailsPrint listonly
	Sleep 333
!macroend

!define REG_KEY_DOTNET_FRAMEWORK `'SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full' 'Release'`

Section
	BringToFront # Just to be sure!

	!insertmacro PrintStatusMessage "Detecting operating system, please wait..."
	${IfNot} ${AtLeastBuild} 7601
		MessageBox MB_ICONSTOP|MB_TOPMOST "This application runs on Windows 7 (SP1) or later!"
		Quit
	${EndIf}

	!insertmacro PrintStatusMessage "Detecting installed .NET Framework version, please wait..."
	ClearErrors
	ReadRegDWORD $0 HKLM64 ${REG_KEY_DOTNET_FRAMEWORK}
	${If} ${Errors}
	${OrIfNot} $0 U> 0
		ReadRegDWORD $0 HKLM32 ${REG_KEY_DOTNET_FRAMEWORK}
	${EndIf}
	${If} $0 U> 0
		DetailPrint "Installed release: $0"
		${IfThen} $0 >= 461808 ${|} Goto launch_application ${|}
	${Else}
		DetailPrint ".NET Framework not found!"
	${Endif}

	!insertmacro PrintStatusMessage "Installing .NET Framework 4.7.2, please wait..."
	${Do}
		ClearErrors
		ExecShellWait /ALLOWERRORUI "open" "$EXEDIR\ndp472-kb4054531-web.exe" /passive
		${IfNot} ${Errors}
		${OrIf} ${Cmd} `MessageBox MB_ICONEXCLAMATION|MB_RETRYCANCEL|MB_TOPMOST "Failed to launch .NET Framework installer!" IDCANCEL`
			${ExitDo}
		${EndIf}
	${Loop}

launch_application:
	!insertmacro PrintStatusMessage "Launching the application, please wait..."
	${GetParent} $EXEDIR $1
	SetOutPath "$1"
	ExecShell /ALLOWERRORUI "open" "$OUTDIR\slunkcrypt-gui.exe"
SectionEnd
