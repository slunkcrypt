#!/bin/sh
if [ -n "$1" ]; then CC="$1"; elif [ -z "$CC" ]; then CC="cc"; fi

if [ -e /etc/os-release ]; then
	PLATFORM_NAME="`cat /etc/os-release | egrep '^PRETTY_NAME=' | cut -c 13- | tr -d '\042'`"
elif [ -x /usr/bin/sw_vers ]; then
	PLATFORM_NAME="`/usr/bin/sw_vers -productName` `/usr/bin/sw_vers -productVersion`"
fi

if [ -z "$PLATFORM_NAME" ]; then
	PLATFORM_NAME="`uname -s -r -v -m`"
fi

echo "Built    : `date -u +'%Y-%m-%d %H:%M:%S'`"
echo "Platform : $PLATFORM_NAME"
echo "Target   : `$CC -dumpmachine`"
echo "Compiler : `$CC -v 2>&1 | egrep -i '(gcc|clang)[[:space:]]+version' 2>/dev/null | head -n 1`"
