#!/bin/bash
set -e
cd -- "$(dirname -- "${BASH_SOURCE[0]}")/../../.."

function mk_slunk() {
	local command="make -B CC=${2}-gcc STRIP=1 FLTO=1 THREAD=0"
	$BASH -x -c "${command}${3:+ ${3}}"
	cp -vf "frontend/bin/slunkcrypt" "out/slunkcrypt-${1}"
}

if [ "$OSTYPE" != "gnu" ]; then
	echo "This script is supposed to run on the gnu/hurd platform !!!"
	exit 1
fi

rm -rf "out" && mkdir -p "out"

if [ "$(arch)" == "x86_64" ]; then
	mk_slunk "x86_64" "x86_64-gnu" "MARCH=x86-64 MTUNE=nocona"
else
	mk_slunk "i686" "i686-gnu" "MARCH=pentiumpro MTUNE=pentium3"
fi

./etc/build/build_info.sh "gcc" > "out/.build_info"

printf "\033[1;32m\nBuild completed successfully.\033[0m\n\n"
