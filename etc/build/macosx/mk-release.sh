#!/bin/zsh
set -e
cd -- "$(dirname -- "${(%):-%N}")/../../.."

if [ -z "${cc_path}" ]; then
	cc_path="/usr/bin/cc"
fi

mk_slunk() {
	make -B CC="${cc_path}" TARGET="${1}-apple-darwin" FLTO=1
	strip -o "out/slunkcrypt-${1}" "frontend/bin/slunkcrypt"
}

rm -rf "out" && mkdir -p "out"

mk_slunk "x86_64"
mk_slunk "arm64"

./etc/build/build_info.sh "${cc_path}" > "out/.build_info"

echo "Build completed successfully."
