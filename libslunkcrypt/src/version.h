/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#ifndef INC_SLUNKCRYPT_VERSION_H
#define INC_SLUNKCRYPT_VERSION_H

#define LIB_VERSION_MAJOR 1
#define LIB_VERSION_MINOR 3
#define LIB_VERSION_PATCH 2

#endif
