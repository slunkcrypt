/******************************************************************************/
/* SlunkCrypt, by LoRd_MuldeR <MuldeR2@GMX.de>                                */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

/* Internal */
#include "debug.h"

/* CRT */
#include <stdio.h>
#include <stdarg.h>

/* logging sub-system */
#ifdef _WIN32
#  define WIN32_LEAN_AND_MEAN 1
#  include <Windows.h>
#else
#  include <syslog.h>
#endif

void slunkcrypt_debug_write(const char* const message)
{
#ifdef _WIN32
	OutputDebugStringA(message);
#else
	syslog(LOG_ALERT, "%s", message);
#endif
}

void slunkcrypt_debug_print(const char* const message, ...)
{
#ifdef _WIN32
	char buffer[100U];
#endif
	va_list args;
	va_start(args, message);
#ifdef _WIN32
	if (vsnprintf(buffer, 100U, message, args) > 0)
	{
		OutputDebugStringA(buffer);
	}
#else
	vsyslog(LOG_ALERT, message, args);
#endif
	va_end(args);
}
